import random

def convertLetters(text):

    LettreSupp = ("!", "#", "$", "%", "&", "'", "(", ")", "*", "+", ",", "-", ".", "/", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", ":", ";", "<", "=", ">", "?", "@", "[", "]", "^", "_", "`", "{", "|", "}", "~", "†", "°", "¢", "£", "§", "•", "¶", "ß", "®", "©", "™", "´", "¨", "≠", "Æ", "Ø", "∞", "±", "≤", "≥", "¥", "∂", "∑", "∏", "π", "∫", "ª", "º", "Ω", "ø", "ƒ", "∆", "«", "Œ", "–", "î", "“", "”", "‘", "’", "÷", "„", "‰", "ı", "ˆ", "˜", "¯", "˘", "˙", "˚", "¸", "⁄", "€", "‹", "›", "ﬁ", "ﬂ", "‡", "·", "‚", '"')
    LettreA = ("Ä", "Å", "á", "à", "â", "ä", "ã", "å", "â", "à", "Ã", "Á")
    LettreE = ("é", "è", "é", "è", "ê", "ë", "Ê", "Ë", "È", "É")
    LettreO = ("ó", "ò", "ô", "ö", "õ", "Õ", "Ó", "Ô", "Ò", "Ö")
    LettreU = ("ú", "ù", "û", "ü", "Ú", "Û", "Ù", "Ü")
    LettreI = ("í", "ì", "î", "ï", "Í", "Î", "Ï", "Ì")

    for x in range(0, 100):
        text = text.replace(LettreSupp[x], "")

    for a in range(0, 11):
        text = text.replace(LettreA[a], "a")

    for e in range(0, 9):
        text = text.replace(LettreE[e], "e")

    for o in range(0, 9):
        text = text.replace(LettreO[o], "o")

    for u in range(0, 7):
        text = text.replace(LettreU[u], "u")

    for i in range(0, 7):
        text = text.replace(LettreI[i], "i")

    text = text.replace("ç", "c")
    text = text.upper()
    text = text.replace(" ", "")

    return text

def  generateKey():

    key = ""
    caracteres = "AZERTYUIOPQSDFGHJKLMWXCVBN"

    while len(key) < 26:
        lettre = ""
        lettre = caracteres[random.randint(0, len(caracteres) - 1)]
        if key.count(lettre) == 0:
            key = key + lettre

    return key

def keyOK(key):

    Alphabet = "AZERTYUIOPQSDFGHJKLMWXCVBN"

    for i in range(0, 25):
        if key.count(Alphabet[i]) != 1:
            return False
    return True

def shiftLeft(keyLeft,i):

    part1 = ""
    part2 = ""

    part1 = keyLeft[0:i]
    part2 = keyLeft[i:26]

    keyLeft = ""
    keyLeft = part2 + part1

    part1 = ""
    part2 = ""

    part1 = keyLeft[0:1]
    part2 = keyLeft[2:26]

    lettre = ""
    lettre = keyLeft[1]

    keyLeft = ""
    keyLeft = part1 + part2

    part1 = ""
    part2 = ""

    part1 = keyLeft[0:13]
    part2 = keyLeft[13:25]

    keyLeft = ""
    keyLeft = part1 + lettre + part2

    return keyLeft

def shiftRight(keyRight,i):

    part1 = ""
    part2 = ""

    part1 = keyRight[0:i+1]
    part2 = keyRight[i+1:26]

    keyRight = ""
    keyRight = part2 + part1

    part1 = ""
    part2 = ""

    part1 = keyRight[0:2]
    part2 = keyRight[3:26]

    lettre = ""
    lettre = keyRight[2]

    keyRight = ""
    keyRight = part1 + part2

    part1 = ""
    part2 = ""

    part1 = keyRight[0:13]
    part2 = keyRight[13:25]

    keyRight = ""
    keyRight = part1 + lettre + part2

    return keyRight

def déchifrage(text,keyLeft,keyRight):
    x = len(text)

    for y in range(0, x):
        part1 = ""
        part2 = ""
        lettre = ""
        i = 0
        lettre = text[y]
        i = keyLeft.index(lettre)
        lettre = ""
        lettre = keyRight[i]
        part1 = text[0:y]
        part2 = text[y + 1:x]
        text = ""
        text = part1 + lettre + part2
        keyLeft = shiftLeft(keyLeft, i)
        keyRight = shiftRight(keyRight, i)

    return text

def chifrage(text, keyRight, keyLeft):
    x = len(text)

    for y in range(0, x):
        part1 = ""
        part2 = ""
        lettre = ""
        i = 0
        lettre = text[y]
        i = keyRight.index(lettre)
        lettre = ""
        lettre = keyLeft[i]
        part1 = text[0:y]
        part2 = text[y + 1:x]
        text = ""
        text = part1 + lettre + part2
        keyLeft = shiftLeft(keyLeft, i)
        keyRight = shiftRight(keyRight, i)

    return text

def algorithm1(text,keyLeft,keyRight,cipher):

    try:
        cipher = int(input("Pour chiffrer votre text rentrer la valeur 1 et pour déchiffrer votre text rentrer la valeur 2 : "))
    except:
        cipher = int(input("Pour chiffrer votre text rentrer la valeur 1 et pour déchiffrer votre text rentrer la valeur 2 : "))

    if cipher == 1:
        print("")

        try:
            print("Voulez vous générer les clefs de cryptages aléatoirement ?")
            keyAlea = int(input("non = 1;  oui = 2 : "))
        except:
            print("Voulez vous générer les clefs de cryptages aléatoirement ?")
            keyAlea = int(input("non = 1;  oui = 2 : "))

        if keyAlea == 1:
            print("")
            verifKR = False
            while not verifKR:
                keyRight = input("KeyRight = ")
                if not keyOK(keyRight):
                    print("Invalid Key")
                    print("")
                else:
                    verifKR = True
            verifKL = False
            while not verifKL:
                keyLeft = input("KeyLeft = ")
                if not keyOK(keyLeft):
                    print("Invalid Key")
                    print("")
                else:
                    verifKL = True
            print("")
            text = input("Rentrer le text à chiffrer : ")
            text = convertLetters(text)
            print("")
            print("-----------------------------------------------")
            text = chifrage(text, keyRight, keyLeft)
            print("")
            print("Voici le text chiffrer : ", text)

        elif keyAlea == 2:
            keyLeft = generateKey()
            keyRight = generateKey()
            print("")
            print("keyRight = ", keyRight)
            print("keyLeft = ", keyLeft)
            print("")
            text = input("Rentrer le text à chiffrer : ")
            text = convertLetters(text)
            print("")
            print("-----------------------------------------------")
            text = chifrage(text, keyRight, keyLeft)
            print("")
            print("Voici le text chiffrer : ", text)

    elif cipher == 2:
        print("")
        verifKR = False
        while not verifKR:
            keyRight = input("KeyRight = ")
            if not keyOK(keyRight):
                print("Invalid Key")
                print("")
            else:
                verifKR = True
        verifKL = False
        while not verifKL:
            keyLeft = input("KeyLeft = ")
            if not keyOK(keyLeft):
                print("Invalid Key")
                print("")
            else:
                verifKL = True
        print("")
        text = input("Rentrer le text à déchiffrer : ")
        text = convertLetters(text)
        print("")
        print("-----------------------------------------------")
        text = déchifrage(text, keyLeft, keyRight)
        print("")
        print("Voici le text déchiffrer : ", text)

text = ""
keyRight = ""
keyLeft = ""
cipher = 0
algorithm1(text, keyLeft, keyRight, cipher)