# Projet d'arithmétique par Thomas Dazy et Kélio Bernard (c)


# >>> Liste des variables:
# > « n » : entier représentant le nombre de lignes.
# > « offset » : entier représentant le décalage.
# > « text » : chaîne de caractères.
# > « l » : entier égal au nombre de caractères de la chaîne « text ».
# > « coordinates » : liste de t-uples de deux entiers.
# > « dictionary » : dictionnaire permettant le chiffrement ou le déchiffrement.
# > « cipher » : un booléen
# > « display » : un booléen.


# >>> Les fonctions

# > Une fonction « keyOK(n,offset) » qui retourne True si n et offset forment une clé valide et False sinon.
def keyOK(n, offset):
    #La clé de cet algorithme est la donnée d’un entier 𝑛 supérieur ou égal à 2 et d’un entier 𝑜𝑓𝑓𝑠𝑒𝑡
    # vérifiant 0 ≤ 𝑜𝑓𝑓𝑠𝑒𝑡 ≤ 2𝑛 − 3.
    if n >= 2:
        if 0 <= offset <= ((2 * n) - 3):
            return True
    return False

# > Une fonction « computeCoordinates(n,l,offset) » qui retourne dans l’ordre et dans une liste de t-uples les coordonnées (𝑖, 𝑗) des différents « points » de la vague.
def computeCoordinates(n, l, offset):

    # On prend l'offfset inférieur au nombre de ligne ou offset[n]
    realOffset = offset
    while realOffset > n:
        realOffset = offset - n

    # `y` est la variable qui stoque la ligne actuelle

    # Liste qui stocke les t-uples de coordonnées (coordinates)
    liste = []

    # Variable utilisées pour le calcul des t-uples
    if offset > n:
        sensDeCalcul = 1
        # y = n - offset - 1
        y = n - offset
    else:
        sensDeCalcul = 0        # 0 = vers le bas et 1 = vers le haut
        y = (offset - 1) - (n-1)

    if y < 0:
        y = (n + y) - 1

    # Pour chaque caractère de "text"
    for x in range(0, l, 1):
        # Si on descend et que la ligne suivante est toujours une ligne bonne:
        if sensDeCalcul == 0 and y + 1 <= n - 1:
            y = y + 1
        # Si on descend et que la ligne suivante n'est pas bonne:
        elif sensDeCalcul == 0 and y + 1 > n - 1:
            sensDeCalcul = 1
            y = y - 1
        elif sensDeCalcul == 1 and y - 1 >= 0:
            y = y - 1
        elif sensDeCalcul == 1 and y - 1 < 0:
            sensDeCalcul = 0
            y = y + 1

        newListe = [x, y]
        liste.append(newListe)
    return liste

# > Une fonction « init(n,l,coordinates) » qui retourne un dictionnaire dont les clés sont les éléments de la liste retournée par la fonction « computeCoordinates(n,l,offset) » et dont toutes les valeurs sont égales à None.
def init(n, l, coordinates):
    dictionary = dict()
    for x in range(0, len(coordinates), 1):
        coordonateX = coordinates[x][0]
        coordonateY = coordinates[x][1]
        intermediaireDict = dict()
        intermediaireDict = dict(coorX=coordonateX, coorY=coordonateY, text="None")
        dictionary[x] = intermediaireDict
        # print("X: ", coordonateX, " - Y:", coordonateY)
        # print (dictionary)
    return dictionary

# > Une procédure « fullDictionaryCipher(text,l,coordinates,dictionary) » où text est ici un message en clair, et qui modifie les valeurs de dictionary pour que celles-ci soient égales aux lettres de text.
def fullDictionaryCipher(text, l, coordinates, dictionary):
    for x in range(0, len(dictionary), 1):
        dictionary[x]["text"] = text[x]
    return dictionary

# > Une procédure « fullDictionaryDecipher(text,n,l,coordinates,dictionary) » où text est ici un message chiffré, et qui modifie les valeurs de dictionary pour que celles-ci soient égales aux lettres de text.
def fullDictionaryDecipher(text, l, coordinates, dictionary):
    index = 0
    for x in range(0, n, 1):
        for y in range(0, l, 1):
            if dictionary[y]['coorY'] == x:
                dictionary[y]['text'] = text[index]
                index = index + 1
    # print("DICTIONARY:")
    # print(dictionary)
    return dictionary

# > Une procédure « fullDictionary(text,n,l,coordinates,dictionary,cipher) » qui appellera l’une des deux procédures précédentes selon que la valeur de cipher soit égale à True ou False.
def fullDictionary(text, n, l, coordinates, dictionary, cipher):
    if cipher == True:
        dictionary = fullDictionaryCipher(text, l, coordinates, dictionary)
    else:
        dictionary = fullDictionaryDecipher(text, l, coordinates, dictionary)
    return dictionary

# > Une procédure « displayDictionary(n,l,coordinates,dictionary) » qui affiche sur la console sous forme de vagues le contenu de dictionary.
def displayDictionary(n, l, coordinates, dictionary):
    print("")
    print("")
    print("Affichage sous forme de vague:")
    print("")

    for x in range(0, n, 1):
        print(x, ": ", end='')
        for y in range(0, l, 1):
            if dictionary[y]['coorY'] == x:

                print(dictionary[y]['text'], end='')
            else:
                print(" ", end='')
        print(sep='')

    print("")
    print("")

# > Une fonction « dictionaryToStringCipher(n,l,coordinates,dictionary) » qui retourne une chaîne de caractères correspondant au chiffrement d’un message dont les lettres sont mémorisées dans dictionary.
def dictionaryToStringCipher(n, l, coordinates, dictionary):
    stringToReturn = ""
    for x in range(0, n, 1):
        for y in range(0, l, 1):
            if dictionary[y]['coorY'] == x:
                stringToReturn = ''.join([stringToReturn, dictionary[y]['text']])
    return stringToReturn

# > Une fonction « dictionaryToStringDecipher(n,l,coordinates,dictionary) » qui retourne une chaîne de caractères correspondant au déchiffrement d’un message dont les lettres sont mémorisées dans dictionary.
def dictionaryToStringDecipher(n, l, coordinates, dictionary):
    stringToReturn = ""
    for x in range(0, len(dictionary), 1):
        stringToReturn = ''.join([stringToReturn, dictionary[x]['text']])
        # print(dictionary[x]['text'], "-")
    return stringToReturn

# > Une fonction « dictionaryToString(n,l,coordinates,dictionary,cipher) » qui appellera l’une des deux fonctions précédentes selon que la valeur de cipher soit égale à True ou False.
def dictionaryToString(n, l, coordinates, dictionary, cipher):
    if cipher == True:
        stringCipher = dictionaryToStringCipher(n, l, coordinates, dictionary)
    else:
        stringCipher = dictionaryToStringDecipher(n, l, coordinates, dictionary)
    return stringCipher

# > Une fonction « algorithm2(text,n,offset,cipher,display) » qui retourne la chaîne text une fois chiffrée ou déchiffrée par l’algorithme selon que la valeur de cipher soit égale à True ou False. Elle affichera également les vagues ou non selon que la valeur de display soit égale à True ou False.
def algorithm2(text, n, offset, cipher, display):
    text = text.replace(" ", "")
    text = text.upper()
    l = len(text)

    coordinates = computeCoordinates(n, l, offset)
    dictionary = init(n, l, coordinates)
    dictionary = fullDictionary(text, n, l, coordinates, dictionary, cipher)

    if display == True:
        displayDictionary(n, l, coordinates, dictionary)

    stringCipher = dictionaryToString(n, l, coordinates, dictionary, cipher)

    print("-----------------------------------------------")

    if cipher == True:
        print("Texte crypté: ")
    else:
        print("Texte décrypté: ")
    print(stringCipher)

    print("-----------------------------------------------")

    print("Éléments de cryptage:")
    print("Nombre de lignes: ", n)
    print("Offset: ", offset)

    print("-----------------------------------------------")
    print("")

# > Fonction qui affiche 100 lignes vides et une barre de séparation
def separator():
    print("\n"*100)
    print("-----------------------------------------------")


# >>> Différentes valeurs nécessaires au fonctionnement du programme:
# Entrées de l'utilisateur
separator()
n = eval(input("Nombre de ligne ?"))
separator()
offset = eval(input("Offset ?"))
separator()
cipher = eval(input("Est-ce que vous voulez cryper (1) ou décrypter (0) ?"))
separator()

# On formatte la variable cipher et on affiche le bon texte
if cipher == 1:
    cipher = True
    text = input("Texte à crypter ?")
elif cipher == 0:
    cipher = False
    text = input("Texte à décrypter ?")

separator()

display = eval(input("Est-ce que vous voulez afficher les vagues (1) ou ne pas les afficher (0) ?"))

# On formatte la variable display
if display == 1:
    display = True
elif display == 0:
    display = False

separator()

# On fait fonctionner le programme
algorithm2(text, n, offset, cipher, display)
